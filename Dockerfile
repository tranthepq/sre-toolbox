FROM dtzar/helm-kubectl:3.9.1

ARG YQ_VERSION=v4.17.2

# yq - a lightweight and portable command-line YAML processor
# https://github.com/mikefarah/yq
RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64.tar.gz -O - | tar xz \
  && mv yq_linux_amd64 /usr/bin/yq 

RUN apk add --no-cache gomplate jq make gettext vim tree

# Devspace 
# https://devspace.sh/cli/docs/getting-started/installation
RUN curl -s -L "https://github.com/loft-sh/devspace/releases/latest" | \
  sed -nE 's!.*"([^"]*devspace-linux-amd64)".*!https://github.com\1!p' | \
  xargs -n 1 curl -L -o devspace \
  && chmod +x devspace \
  && mv devspace /usr/bin/devspace

# AWSCLI and aws-iam-authenticator
# https://stackoverflow.com/questions/60298619/awscli-version-2-on-alpine-linux
ENV GLIBC_VER=2.31-r0

# install glibc compatibility for alpine
RUN apk --no-cache add \
  binutils \
  curl \
  && curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
  && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk \
  && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk \
  && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-i18n-${GLIBC_VER}.apk \
  && apk add --no-cache \
  glibc-${GLIBC_VER}.apk \
  glibc-bin-${GLIBC_VER}.apk \
  glibc-i18n-${GLIBC_VER}.apk \
  && /usr/glibc-compat/bin/localedef -i en_US -f UTF-8 en_US.UTF-8 \
  && curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
  && unzip awscliv2.zip \
  && aws/install \
  && rm -rf \
  awscliv2.zip \
  aws \
  /usr/local/aws-cli/v2/current/dist/aws_completer \
  /usr/local/aws-cli/v2/current/dist/awscli/data/ac.index \
  /usr/local/aws-cli/v2/current/dist/awscli/examples \
  glibc-*.apk \
  && find /usr/local/aws-cli/v2/current/dist/awscli/botocore/data -name examples-1.json -delete \
  && apk --no-cache del \
  binutils \
  && rm -rf /var/cache/apk/*
RUN curl -o aws-iam-authenticator https://s3.us-west-2.amazonaws.com/amazon-eks/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator \
  && chmod +x ./aws-iam-authenticator \
  && mv aws-iam-authenticator /usr/local/bin/

# trick to keep container running in k8s cluster
# CMD ["sh", "-c", "tail -f /dev/null"]
