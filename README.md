# SRE toolbox

A Docker image `pcloud/sre-toolbox` contains essential tools for SRE tasks

## Features

- `helm`: https://helm.sh/
- `kubectl`: https://kubernetes.io/docs/tasks/tools/#kubectl
- `yq`: https://github.com/mikefarah/yq
- `jq`: https://stedolan.github.io/jq/
- `devspace`: https://devspace.sh/
- `make`: https://www.gnu.org/software/make
- `gomplate`: https://docs.gomplate.ca/
- `envsubst`: https://www.gnu.org/software/gettext/manual/html_node/envsubst-Invocation.html

## Versions

`pcloud/sre-toolbox:0.3`

- kubectl `1.23.0`
- yq `4.17.2`
- jq `jq-master-v3.15.0_alpha20210804-4073-gb39e1241e8`
- devspace `5.18.4`
- make `4.3`
- gomplate `3.10.0`
- envsubst `0.21`
- aws `2.5.4`
- aws-iam-authenticator `v0.5.0`

`pcloud/sre-toolbox:0.2`

- helm `v3.7.2`
- kubectl `1.23.0`
- yq `4.17.2`
- jq `jq-master-v3.15.0_alpha20210804-4073-gb39e1241e8`
- devspace `5.18.2`
- make `4.3`
- gomplate `3.10.0`
- envsubst `0.21`

`pcloud/sre-toolbox:0.1`

- helm `3.4.0`
- kubectl `1.19.0`
- yq `4.2.0`
- jq `jq-master-v20200428-28-g864c859e9d`
- devspace `5.17.0`
- make `4.3`
- gomplate `3.7.0`
- envsubst `0.20.2`

## Run Locally

Build Docker image

```bash
  make build
```

Quick run Docker container

```bash
  make run
```

## Run in K8S cluster

`deployment.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sre
  namespace: default
  labels:
    app: toolbox
spec:
  selector:
    matchLabels:
      app: toolbox
  template:
    metadata:
      labels:
        app: toolbox
    spec:
      containers:
        - name: toolbox
          image: pcloud/sre-toolbox:0.3
          command: ["sleep"]
          args: ["infinity"]
```

```bash
# create deployment
kubectl apply -f deployment.yaml

# exec into container bash shell
kubectl exec -it pod/<name> -- /bin/bash

# clean up
kubectl delete -f deployment.yaml
```
